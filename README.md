# pipeline-tp1



```
cd existing_repo
git remote add origin https://gitlab.com/perrineosw/pipeline-tp1.git
git branch -M main
git push -uf origin main
```

## Docker

- create an image : `docker build -t ynov .`
- fixe a port : `docker run -d -p 8090:80 ynov`
- web link : `http://localhost:8090/`
- gitlab login : `docker login registry.gitlab.com`
- docker pull : `docker pull registry.gitlab.com/perrineosw/pipeline-tp1:main` (play with docker)
